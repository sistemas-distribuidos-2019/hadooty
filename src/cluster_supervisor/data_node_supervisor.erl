-module(data_node_supervisor).
-export([start/1, stop/1]).

%Server
stop(Pid) ->
  Pid ! stop.

start(Node) ->
  spawn_link(fun() -> init(Node) end).

init(Node) ->
  {ok, FreeSpace, Files} = data_server:ping(Node),
  loop(Node, FreeSpace, Files).

loop(Node, FreeSpace, Files) ->
  receive
    stop ->
      lager:info("Stopped")
  after 3000 ->
    {NewFreeSpace, NewFiles} = check_for_metadata_change(Node, FreeSpace, Files),
    loop(Node, NewFreeSpace, NewFiles)
  end.

%Private
check_for_metadata_change(Node, FreeSpace, Files) ->
  case data_server:ping(Node) of
    {error, timeout} ->
      replica_managment_server:node_down(Node),
      {FreeSpace, Files};
    {ok, NewFreeSpace, NewFiles} ->
      compare_metadata(Node, FreeSpace, Files, NewFreeSpace, NewFiles)
  end.

compare_metadata(Node, OldFreeSpace, OldFiles, NewFreeSpace, NewFiles) ->
  case (OldFreeSpace =:= NewFreeSpace) and (OldFiles =:= NewFiles)  of
    true -> {OldFreeSpace, OldFiles};
    false ->
      replica_managment_server:node_changed(Node, NewFreeSpace, NewFiles),
      {NewFreeSpace, NewFiles}
  end.