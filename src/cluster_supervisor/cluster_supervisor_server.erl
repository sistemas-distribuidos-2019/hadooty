-module(cluster_supervisor_server).

%% API
-export([start/0, stop/0, register_node/1, delete_node/1]).

%Client
cast(Request) ->
  global:whereis_name(hadooty_cluster_supervisor) ! Request.

register_node(Node) ->
  cast({register, Node}).

delete_node(Node) ->
  cast({delete, Node}).

%Server
stop() ->
  global:whereis_name(hadooty_cluster_supervisor) ! stop.

start() ->
  Name = hadooty_cluster_supervisor,
  Pid = spawn(fun() -> init() end),
  global:register_name(Name, Pid).

init() ->
  loop([]).

loop(State) ->
  receive
    {register, Node} ->
      lager:info("Registering new node ~p", [Node]),
      Supervisor = data_node_supervisor:start(Node),
      loop([{Supervisor, Node} | State]);
    {delete_node, Node} ->
      lager:info("Deleting node ~p", [Node]),
      case lists:keytake(Node, 2, State) of
        false ->
          lager:info("Node ~p not found", [Node]), 
          loop(State);
        {value, {Supervisor, Node}, NewState} -> 
          data_node_supervisor:stop(Supervisor),
          loop(NewState)
      end;
    stop ->
      lager:info("Stopped")
  end.
