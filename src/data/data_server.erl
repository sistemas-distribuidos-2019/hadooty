-module(data_server).
-export([stop/1, start/1, ping/1, save/3, get/2, remove/2]).

call_data_node(Node, Request) ->
  Node ! Request,
  receive
    Response -> Response
  after 10000 ->
    {error, timeout}
  end.

ping(Node) ->
  lager:info("Sending ping"),
  call_data_node(Node, {ping, self()}).

save(Node, FileName, FileData) ->
  call_data_node(Node, {save, FileName, FileData, self()}).

get(Node, FileName) ->
  call_data_node(Node, {get, FileName, self()}).

remove(Node, FileName) ->
  call_data_node(Node, {remove, FileName, self()}).

% Server
stop(Node) ->
  Node ! stop.

start(InitialSpace) ->
  Name = data_node,
  Pid = spawn(fun() -> init(InitialSpace) end),
  register(Name, Pid),
  Name.

init(InitialSpace) ->
  loop(data:init(InitialSpace)).

loop(State) ->
  receive
    {ping, From} ->
      lager:info("Receiving ping from ~p", [From]),
      From ! {ok, data:available_space(State), data:files_names(State)},
      loop(State);
    {save, FileName, FileData, From} ->
      case data:add(FileName, FileData, State) of
        not_enough_space -> 
          From ! {error, not_enough_space},
          loop(State);
        NewState -> 
          From ! ok,
          loop(NewState)
      end;
    {get, FileName, From} ->
      case data:get(FileName, State) of
        file_not_found -> From ! {error, file_not_found};
        {ok, FileName, FileData} -> From ! {ok, FileName, FileData}
      end,
      loop(State);
    {remove, FileName, From} -> 
      case data:remove(FileName, State) of
        file_not_found -> 
          From ! {error, file_not_found},
          loop(State);
        {ok, NewState} ->
          From ! ok,
          loop(NewState)
      end;
    stop ->
      lager:info("Stopped")
  end.