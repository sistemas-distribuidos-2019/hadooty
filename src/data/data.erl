-module(data).
-export([init/1, add/3, remove/2, available_space/1, get/2, files_names/1]).
-include("data.hrl").

%Recibe un InitialSpace en bytes
init(InitialSpace) ->
  #data{space=InitialSpace, files=[]}.

add(FileName, FileData, #data{space=AvailableSpace, files=Files}) ->
  case calculate_available_space(AvailableSpace, byte_size(FileData)) of
    not_enought_space -> not_enought_space;
    NewAvalilableSpace ->
      File = #file{name=FileName, file_data=FileData},
      #data{space=NewAvalilableSpace, files=[File | Files]}
  end.

calculate_available_space(AvailableSpace, NeededSpace) ->
  case AvailableSpace - NeededSpace of
    NewAvalilableSpace when NewAvalilableSpace =< 0 -> not_enought_space;
    NewAvalilableSpace when NewAvalilableSpace > 0 -> NewAvalilableSpace      
  end.

get(FileName, #data{files=Files, _=_}) ->
  case lists:keyfind(FileName, #file.name, Files) of
      false -> file_not_found;
      #file{name=FileName, file_data=FileData} ->
        {ok, FileName, FileData}
  end.    

remove(FileName, #data{space=AvailableSpace, files=Files}) ->
  case lists:keytake(FileName, #file.name, Files) of
    false -> file_not_found;
    {value, #file{file_data=FileData, _=_}, NewFiles} ->
      NewAvalilableSpace = AvailableSpace + byte_size(FileData),
      {ok, #data{space=NewAvalilableSpace, files=NewFiles}}
  end.

available_space(#data{space=AvailableSpace, _=_}) ->
  AvailableSpace.

files_names(#data{files=Files, _=_}) ->
  lists:map(fun(#file{name=Name, _=_}) -> Name end, Files).