%%%-------------------------------------------------------------------
%% @doc hadooty public API
%% @end
%%%-------------------------------------------------------------------

-module(hadooty_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
  lager:start(),
  hadooty_sup:start_link().

stop(_State) ->
  ok.

%% internal functions
