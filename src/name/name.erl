-module(name).
-export([init/0, add/3, remove/2, nodes_by_file/2, files_by_node/2, replace_node/4]).
-include("name.hrl").
-define(root(), "/").

init() ->
  #dir{name=?root(), childs=[]}.

add([], _, _) ->
  {error, no_path};
add([?root() | []], _, _) ->
  {error, existent_path};
add([Dir | [FileName | []]], Nodes, #dir{name=Dir, childs=Childs}) ->
  case existent_name(FileName, Childs) of
    false ->
      File = #file{name=FileName, nodes=Nodes},
      {ok, #dir{name=Dir, childs=[File | Childs]}};
    true -> {error, existent_path}
  end;
add([_Dir | [_FileName | []]], _Nodes, #file{name=_, nodes=_}) ->
  {error, existent_path};
add([Step | Rest], Nodes, #dir{name=Step, childs=Childs}) ->
  Results = lists:map(fun(Child) -> add(Rest, Nodes, Child) end, Childs),
  FoldResults = lists:foldl(fun(Result, NewNames) -> map_result(Result, NewNames) end, {error, path_not_found, []}, Results),
  case map_results(Step, FoldResults) of
    {error, path_not_found, Names} -> add_and_create_path( Rest, Nodes, Names);
    Any -> Any
  end;
add([_Step | _Rest],   _Nodes, #dir{name=_, childs=_} = Names) ->
  {error, path_not_found, Names}.

existent_name(FileName, Childs) ->
  case lists:keyfind(FileName, #dir.name, Childs) of
    false -> 
      case lists:keyfind(FileName, #file.name, Childs) of
        false -> false;
        _ -> true          
      end;
    _ -> true
  end.

map_results(Step, {ok, Results}) ->
  {ok, #dir{name=Step, childs=Results}};
map_results(Step, {error, path_not_found, Results}) ->
  {error, path_not_found, #dir{name=Step, childs=Results}};
map_results(_Step, {error, existent_path}) ->
  {error, existent_path}.

map_result({ok, Child}, {error, path_not_found, Results}) ->
  {ok, [Child | Results]};
map_result({error, path_not_found, Child}, {ok, Results}) ->
  {ok, [Child | Results]};
map_result({error, existent_path}, {error, path_not_found, _Results}) ->
  {error, existent_path};
map_result({error, path_not_found, _Results}, {error, existent_path}) ->
  {error, existent_path}.

add_and_create_path([], _Nodes, _Names) ->
  {error, no_path}; 
add_and_create_path([FileName | []], Nodes, #dir{name=Dir, childs=Childs}) ->
  File = #file{name=FileName, nodes=Nodes},
  {ok, #dir{name=Dir, childs=[File | Childs]}};
add_and_create_path([Step | Rest], Nodes, #dir{name=Dir, childs=Childs}) ->
  NewPath = add_and_create_path2(Rest, Nodes, #dir{name=Step, childs=[]}),
  {ok, #dir{name=Dir, childs=[NewPath | Childs]}}.

add_and_create_path2([FileName | []], Nodes, #dir{name=Dir, childs=[]}) ->
  File = #file{name=FileName, nodes=Nodes},
  #dir{name=Dir, childs=[File]};
add_and_create_path2([Step | Rest], Nodes, #dir{name=Dir, childs=[]}) ->
  NewDir = add_and_create_path2(Rest, Nodes, #dir{name=Step, childs=[]}),
  #dir{name=Dir, childs=[NewDir]}.

remove([?root() | []], #dir{name=?root(), childs=_}=Names) ->
  Names;
remove([FileName | []], #file{name=FileName, nodes=Nodes}) ->
  {ok, Nodes, removed};
remove([Step | Path], #dir{name=Step, childs=Childs}) ->
  Results = lists:map(fun(Child) -> remove(Path, Child) end, Childs),
  case should_self_remove (Step, Results) of
    true -> 
      Nodes = get_nodes_from_results(Results),
      {ok, Nodes, removed};
    false ->
      dir_with_removed_child(Step, Results)
  end;
remove(_Path, Names) ->
  Names.

should_self_remove(Step, Results) ->
  case lists:keyfind(ok, 1, Results) of
    false -> false;
    _ -> (Step =/= ?root()) and (length(Results) =:= 1)
  end.

get_nodes_from_results(Results) ->
  case lists:keyfind(ok, 1, Results) of
    false -> false;
    {ok, Nodes, _} -> Nodes
  end.

removed_child(Results) ->
    case lists:keyfind(ok, 1, Results) of
        false -> false;
        {ok, _Nodes, removed} -> removed;
        {ok, _Nodes, _Names} -> return
      end.
    
remove_removed_child(Results) ->
  lists:keydelete(ok, 1, Results).

from_removed_results(Results) ->
  case lists:keytake(ok, 1, Results) of
    false -> Results;
    {value, {ok, _Nodes, Dir}, Rest} -> [Dir | Rest]
  end.

dir_with_removed_child(Step, Results) ->
  case removed_child(Results) of
    removed ->
      Nodes = get_nodes_from_results(Results),
      {ok, Nodes, #dir{name=Step, childs=remove_removed_child(Results)}};
    return ->
      Nodes = get_nodes_from_results(Results),
      {ok, Nodes, #dir{name=Step, childs=from_removed_results(Results)}};
    false -> #dir{name=Step, childs=Results}
  end.

nodes_by_file([FileName | []], #file{name=FileName, nodes=Nodes}) ->
  {ok, Nodes};
nodes_by_file([Dir | Rest], #dir{name=Dir, childs=Childs}) ->
  Results = lists:map(fun(Child) -> nodes_by_file(Rest, Child) end, Childs),
  lists:foldl(fun map_find_nodes_result/2, {error, not_found}, Results);
nodes_by_file(_Path, _Names) ->
  {error, not_found}.

map_find_nodes_result({ok, Nodes}, _) ->
  {ok, Nodes};
map_find_nodes_result(_, {ok, Nodes}) ->
  {ok, Nodes};
map_find_nodes_result(_, Error) ->
  Error.

files_by_node(Node, #file{name=Name, nodes=Nodes}) ->
  case lists:any(fun(Elem) -> Elem =:= Node end, Nodes) of
    false -> {error, not_found};
    true -> {ok, [[Name]]}
  end;
files_by_node(Node, #dir{name=Name, childs=Childs}) ->
  Results = lists:map(fun(Child) -> files_by_node(Node, Child) end, Childs),
  lists:foldl(fun(Res, Acc) -> map_find_files_result(Res, Acc, Name) end, {error, not_found}, Results).

map_find_files_result({ok, Results}, {error, not_found}, Name) ->
  Mapped = lists:map(fun(Item) -> [Name | Item] end, Results),
  {ok, Mapped};
map_find_files_result({error, not_found}, {ok, Results}, Name) ->
  Mapped = lists:map(fun(Item) -> [Name | Item] end, Results),
  {ok, Mapped};
map_find_files_result({ok, Results1}, {ok, Results2}, Name) ->
  Mapped = lists:append(lists:map(fun(Item) -> [Name | Item] end, Results1), Results2),
  {ok, Mapped};
map_find_files_result({error, not_found}, {error, not_found}, Name) ->
  {error, not_found}.
        
replace_node([FileName | []], NewNode, OldNode, #file{name=FileName, nodes=Nodes}) ->
  {ok, #file{name=FileName, nodes= [NewNode | lists:delete(OldNode, Nodes)]}};
replace_node([Dir | Rest], NewNode, OldNode, #dir{name=Dir, childs=Childs}) ->
  Results = lists:map(fun(Child) -> replace_node(Rest, NewNode, OldNode, Child) end, Childs),
  MappedChilds = lists:foldl(fun map_replace_node_result/2, {error, not_found, []}, Results),
  case MappedChilds of
    {ok, NewChilds} -> {ok, #dir{name=Dir, childs=NewChilds}};
    {error, not_found, NewChilds} -> {error, not_found, #dir{name=Dir, childs=NewChilds}}
  end; 
replace_node(_Path, _NewNode, _OldNode, Names) ->
  {error, not_found, Names}.

map_replace_node_result({error, not_found, Child}, {error, not_found, Childs}) ->
  {error, not_found, lists:append(Childs, [Child])};
map_replace_node_result({error, not_found, Child}, {ok, Childs}) ->
  {ok, lists:append(Childs, [Child])};
map_replace_node_result({ok, Child}, {error, not_found, Childs}) ->
  {ok, lists:append(Childs, [Child])}.