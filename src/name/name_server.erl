-module(name_server).
-export([start/0, stop/0, files/1, nodes/1, replace/3]).

%Client
call(Request) ->
  global:whereis_name(hadooty_name_server) ! Request,
  receive
    Response -> Response
  after 10000 ->
    lager:info("Request timeout"),
    {error, timeout}
  end.

files(Node) ->
  call({files, Node, self()}).

nodes(File) ->
  call({nodes, File, self()}).

replace(File, NewNode, OldNode) ->
  call({replace, File, NewNode, OldNode, self()}).

%Server
start() ->
  Name = hadooty_name_server,
  Pid = spawn(fun() -> init() end),
  global:register_name(Name, Pid).
  
stop() ->
   gen_server:call(name_node, stop).

init() ->
  loop(name:init()).

loop(State) ->
  receive
    {files, Node, From} -> 
      lager:info("Files request to ~p from ~p", [Node, From]),
      From ! name:files_by_node(Node, State),
      loop(State);
    {nodes, File, From} ->
      lager:info("Nodes request to ~p from ~p", [File, From]),
      From ! name:nodes_by_file(File, State),
      loop(State);
    {replace, File, NewNode, OldNode, From} -> 
      lager:info("Replace request to ~p ~p ~p from ~p", [File, NewNode, OldNode, From]),
      case name:replace_node(File, NewNode, OldNode, State) of
        {ok, NewState} -> 
          From ! ok,
          loop(NewState);
        {error, not_found, _} -> 
          From ! {error, not_found},
          loop(State)
      end
  end.



