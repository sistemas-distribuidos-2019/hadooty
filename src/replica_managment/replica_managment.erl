-module(replica_managment).
-export([init/0, new_data_node/4, delete_data_node/2, node_changed/4, find_node_for_file/3, node_id/1]).
-include("replica_managment.hrl").

init() ->
  [].

new_data_node(Node, FreeSpace, Files, State) ->
  case lists:keyfind(Node, #node.id, State) of
    false -> [#node{id=Node, free_space=FreeSpace, files=Files} | State];
    _ -> State
  end.

delete_data_node(Node, State) ->
  lists:keydelete(Node, #node.id, State).

node_changed(Node, FreeSpace, Files, State) ->
  case lists:keytake(Node, #node.id, State) of
    false -> {error, not_found};
    {value, _OldNode, NewState} -> [#node{id=Node, free_space=FreeSpace, files=Files} | NewState]
  end.

find_node_for_file(FileName, _FileSize, State) ->
  Filtered = lists:filter(fun(#node{files=Files, _=_, _=_}) -> not lists:member(FileName, Files) end, State),
  Sorted = lists:sort(
    fun (#node{free_space=FreeSpace1, _=_, _=_}, #node{free_space=FreeSpace2, _=_, _=_}) ->
      FreeSpace1 =< FreeSpace2
    end, Filtered),
  lists:nth(1, Sorted).

node_id(#node{id=Id, _=_, _=_}) ->
  Id.
