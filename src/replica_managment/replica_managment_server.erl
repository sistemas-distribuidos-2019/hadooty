-module(replica_managment_server).

%% API
-export([start/0, stop/0, register_node/1, node_changed/3, node_down/1, node_for_file/2]).

%Client
cast(Request) ->
  global:whereis_name(hadooty_replica_managment) ! Request.

call(Request) ->
  global:whereis_name(hadooty_replica_managment) ! Request,
  receive
    Response -> Response
  after 10000 ->
    lager:info("Request timeout"),
    {error, timeout}
  end.

register_node(Node) ->
  cast({register, Node}).

node_changed(Node, FreeSpace, Files) ->
  cast({node_changed, Node, FreeSpace, Files}).

node_down(Node) ->
  cast({node_down, Node}).

node_for_file(FileName, FileSize) ->
  call({new_file, FileName, FileSize, self()}).

%Server
stop() ->
  global:whereis_name(hadooty_replica_managment) ! stop.

start() ->
  Name = hadooty_replica_managment,
  Pid = spawn(fun() -> init() end),
  global:register_name(Name, Pid).

init() ->
  loop(replica_managment:init()).

loop(State) ->
  receive
    {register, Node} ->
      lager:info("Registering new node ~p", [Node]),
      {ok, FreeSpace, Files} = data_server:ping(Node),
      cluster_supervisor_server:register_node(Node),
      NewState = replica_managment:new_data_node(Node, FreeSpace, Files, State),
      loop(NewState);
    {new_file, FileName, FileSize, From} ->
      lager:info("Get node for ~p ~p from ~p", [FileName, FileSize, From]),
      Node = replica_managment:find_node_for_file(FileName, FileSize, State),
      From ! replica_managment:node_id(Node),
      loop(State);
    {node_changed, Node, FreeSpace, Files} ->
      lager:info("Metadata changed for node ~p ~p ~p", [Node, FreeSpace, Files]),
      case replica_managment:node_changed(Node, FreeSpace, Files, State) of
        {error, not_found} -> 
          lager:info("Node ~p not found", [Node]),
          loop(State);
        NewState -> loop(NewState)
      end;
    {node_down, Node} ->
      lager:info("Node ~p down", [Node]),
      Files = get_file_by_node(Node),
      NodesByFile = lists:map(fun get_nodes_for_file/1, Files),
      FilesContent = lists:map(fun get_file_content/1, NodesByFile),
      NewFilesByNode = lists:map(fun({FileName, FileContent}) -> save_file(FileName, FileContent, State) end, FilesContent),
      lists:map(fun({FileName, NewNode}) -> name_server:replace(FileName, NewNode, Node) end, NewFilesByNode),
      cluster_supervisor_server:delete_node(Node),
      NewState = replica_managment:delete_data_node(Node, State),
      loop(NewState);
    stop ->
      lager:info("Stopped")
  end.

%Private
get_file_by_node(Node) ->
  case name_server:files(Node) of
    {error, not_found} -> [];
    {ok, Files} -> Files
  end.

get_nodes_for_file(File) -> 
  case name_server:nodes(File) of
    {ok, Nodes} -> {File, Nodes};
    {error, not_found} -> {File, []}
  end.

get_file_content({File, Nodes}) ->
  lists:foldl(fun(Node, Acc) -> reduce_to_file(File, Node, Acc) end, error, Nodes).

reduce_to_file(_, _, {FileName, FileData}) ->
  {FileName, FileData};
reduce_to_file(File, Node, _) ->
  case data_server:get(Node, File) of
    {ok, FileName, FileData} -> {FileName, FileData};
    {error, _} -> error
  end.

save_file(FileName, FileContent, State) -> 
  {ok, Node} =  replica_managment:find_node_for_file(FileName, size(FileContent), State),
  ok = data_server:save(Node, FileName, FileContent), 
  {FileName, Node}.

