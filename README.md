# hadooty

El nodo de metadata tiene:

- Replica management
- Supervisor
- Name server

## Primer solucion

Nodo de metadata con esquema de master y slave para tener redundancia.

_Ventajas_:
- Mas simple la implementacion
	- Menos cantidad de mensajes de sincronizacion en la red

_Desventajas_:
- Solo hay redundancia de 1
	- El nodo slave estara siempre iddle a menos que algo le suceda al master

Comentar el tema de como cambia la velocidad por cercania y lejania entre clientes y nodos del sistema.

## Segunda solucion

Nodo de metadata distribuido con sincronizacion por consenso

Los supervisores se encargan de saber el estado de los nodos de datos. Ademas de saber si estan vivos o no, 
obtienen metadata necesaria (espacio libre, cantidad de archivos, etc).
Para hacer esto pinguean a los nodos de datos cada N tiempo, y a su vez reciben notificaciones del nodo de datos cuando se modifica la informacion.
A su vez los supervisores van a estar sincronizados entre si. Es decir ante cada modificacion, la metadata de los nodos de datos va a propagarse al resto de los supervisores.
Los supervisores tambien estaran informando al replica managment de los cambios de metadata.
La supervicion de los nodos de datos del cluster estara repartida entre los supervisores disponibles, idealmente por cercania de red.

Los name servers se encargan de mantener la informacion del arbol del file system, en cada hoja (archivo) mantendra los nodos que tienen replicado ese archivo. A su vez estos se mantienen sincronizados entre si. Estos van a poder adquirir un lock para un path cuando se quiera escribir un archivo, consensuado entre todos, se mantiene el lock hasta que el cliente informa el commit.
Tambien los name servers reciben notificaciones del replica managment para eliminar o agregar nodos de datos en determinado archivo.

Los replica managment se encargan de mantener un numero de replicas por archivo mas o menos constante y ademas mantener la cantidad de informacion distribuida lo mas uniformemente posible en todos los nodos de datos.
Los replica managment conocen a todos los nodos de datos del cluster y tienen asociada a estos su metadata, que la usaran para balancear la cantidad de informacion por nodo de datos y para elegir que nodos de datos para replicar un archivo.
La metadata que tiene guardada sera actualizada mediante notificacion de su supervisor asociado.
El replica managment recibira requests del cliente pidiendo nodos para escribir, cuando un archivo esta corrupto o un nodo dejo de contestar.

Los clientes se encargaran de llevar a cabo las operaciones sobre el file system.
En el caso de la **escritura**:

	Cuando es un archivo nuevo:
		- El cliente primero obtendra un lock pedido a su name server mas cercano con el nombre del archivo.
		- Le pide a su replica managment mas cercano los nodos donde va a escribir su nuevo archivo
		- Intenta escribir el archivo en los nodos asignados. Si alguno falla lo informa y pide otro mas.
		- Commitea al name server el nombre del archivo junto con los nodos donde escribio.
	
	Cuando es un archivo existente:
		- El cliente primero obtendra un lock pedido a su name server mas cercano con el nombre del archivo.
		- Le pide al name server los nodos donde estaba replicado el archivo
		- Intenta escribir el archivo en los nodos asignados. Si alguno falla lo informa y pide otro mas.
		- Commitea al name server el nombre del archivo.

En el caso de la **lectura**:
	- Le pide a su name server mas cercano los nodos donde esta replicado el archivo
	- Intenta leer el archivo de los nodos. Si alguno falla simplemente lo informa e intenta con el siguiente.
	- Cuando obtiene el archivo valida el checksum provisto por el nodo de datos para verificar la integridad de la informacion, si falla la validacion lo informa.
	
_Ventajas_:
	- Toda la informacion esta distribuida, por lo que le agrega mas resiliencia al cluster.
	- La supervicion de los nodos de datos esta repartida entre los nodos manager por lo que disminuye la carga sobre un manager en particular.
	- Se puede escalar horizontalemente la capasidad de anteder requests del cluster
	- Al no haber un esquema de master slave, todos los managers estan recibiendo requests del cliente (es decir ninguno esta iddle)

_Desventajas_:
- La solucion es mucho mas compleja que la anterior.
- Hay mas trafico de sincronizacion.

NOTA: Comentar que es necesario mantener una estrategia de replicamiento de informacion cuando el sistema esta distribuido a lo largo de distintas redes porque en el caso de que halla una particion de red si la info no esta bien replicada puede haber perdida de informacion de uno de los lados
