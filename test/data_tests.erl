-module(data_tests).
-export([init_test_/0, add_file_test_/0, remove_file_test_/0, available_space_test_/0]).
-include_lib("eunit/include/eunit.hrl").
-include("../src/data/data.hrl").

with_data() ->
  data:init(100).

data_with_no_free_space() ->
  data:init(0).

data_with_file() ->
  FileName = "file name",
  FileData = <<1, 1, 1>>,
  Data = data:add(FileName, FileData, data:init(100)),
  {FileName, FileData, Data}.

%% TESTS %%
the_available_space(Data) ->
  ?_assertEqual(data:available_space(Data), Data#data.space).

add_discount_space(Data) ->
  NewData = data:add("_", <<1>>, Data),
  ?_assertEqual(NewData#data.space, Data#data.space - 1).

add_creates_new_file(Data) ->
  FileName = "new file",
  FileData = <<1, 1, 1, 1>>,
  NewData = data:add(FileName, FileData, Data),
  NewFile = lists:nth(1, NewData#data.files),
  [
    ?_assertEqual(length(NewData#data.files), 1),
    ?_assertEqual(NewFile, #file{name=FileName, file_data=FileData})
  ].

can_not_add_if_there_is_no_space(Data) ->
  ?_assertEqual(data:add("_", <<1>>, Data), not_enought_space).

remove_file_increase_space({FileName, FileData, Data}) ->
  {ok, NewData} = data:remove(FileName, Data),
  ExpectedLength = Data#data.space + byte_size(FileData),
  ?_assertEqual(NewData#data.space, ExpectedLength).

remove_file_deletes_from_file_list({FileName, _FileData, Data}) ->
  {ok, NewData} = data:remove(FileName, Data),
  ?_assertEqual(NewData#data.files, []).
  
can_not_remove_inexistent_file(Data) ->
  ?_assertEqual(data:remove("something", Data), file_not_found).

get_file_returns_it({FileName, FileData, Data}) ->
  ?_assertEqual(data:get(FileName, Data), {ok, FileName, FileData}).

get_unexistent_file_returns_error(Data) ->
  ?_assertEqual(data:get("something", Data), file_not_found).

%% FIXTURES %%
init_test_() ->
  ?_assertEqual(data:init(100), #data{space=100, files=[]}).

add_file_test_() ->
  [{setup, fun with_data/0, fun add_discount_space/1},
  {setup, fun with_data/0, fun add_creates_new_file/1},
  {setup, fun data_with_no_free_space/0, fun can_not_add_if_there_is_no_space/1}].

get_file_test_() ->
  [{setup, fun data_with_file/0, fun get_file_returns_it/1},
  {setup, fun with_data/0, fun get_unexistent_file_returns_error/1}].

remove_file_test_() ->
  [{setup, fun data_with_file/0, fun remove_file_increase_space/1},
  {setup, fun data_with_file/0, fun remove_file_deletes_from_file_list/1},
  {setup, fun with_data/0, fun can_not_remove_inexistent_file/1}].

available_space_test_() ->
  {setup, fun with_data/0, fun the_available_space/1}.
