-module(name_tests).
-export([init_test_/0, add_test_/0, remove_test_/0, nodes_by_file_test_/0, files_by_node_test_/0, replace_node_test_/0]).
-include_lib("eunit/include/eunit.hrl").
-include("../src/name/name.hrl").

init_and_nodes() ->
  {name:init(), [1, 2, 3]}.

init() ->
  {name:init()}.
    
existent_file_and_nodes() ->
  FilePath = ["/", "existent", "file"],
  {ok, Names} = name:add(FilePath, [1, 2, 2], name:init()),
  {FilePath, Names, [1, 2, 3]}.

with_existent_files() ->
  OneFilePath = ["/", "existent", "file1"],
  OneNodes = [1, 2, 4, 5],
  OtherFilePath = ["/", "existent", "file2"],
  OtherNodes = [5, 6, 21, 6],
  
  {ok, Names} = name:add(OneFilePath, OneNodes, name:init()),
  {ok, NewNames} = name:add(OtherFilePath, OtherNodes, Names),

  {{OneFilePath, OneNodes}, {OtherFilePath, OtherNodes}, NewNames}.
    
%%TESTS%%
add_with_empty_path_fails({Names, Nodes}) ->

  ?_assertEqual(name:add([], Nodes, Names), {error, no_path}).

add_in_existent_file_fails({ExistentFilePath, Names, Nodes}) ->
  ?_assertEqual(name:add(ExistentFilePath, Nodes, Names), {error, existent_path}).

cant_add_file_in_root({Names, Nodes}) ->
  ?_assertEqual({error, existent_path}, name:add(["/"], Nodes, Names)).
  
add_with_existent_file_in_path_step_fails({Names, Nodes}) ->
  {ok,NewNames} = name:add(["/", "my"], Nodes, Names),
  ?_assertEqual(name:add(["/", "my", "path"], Nodes, NewNames), {error, existent_path}).

add_siblin({Names, Nodes}) ->
    {ok,NewNames} = name:add(["/", "my", "path"], Nodes, Names),
    ExpectedNames = #dir{name="/", childs=[#dir{name="my", childs=[#file{name="siblin", nodes=Nodes}, #file{name="path", nodes=Nodes}]}]},
    ?_assertEqual({ok, ExpectedNames}, name:add(["/", "my", "siblin"], Nodes, NewNames)).
    
add_in_existent_dir_fails({Names, Nodes}) ->
  {ok, NewNames} = name:add(["/", "my", "path"], Nodes, Names),
  ?_assertEqual({error, existent_path}, name:add(["/", "my"], Nodes, NewNames)).
    
add_in_existent_path({Names, Nodes}) ->
  ?_assertEqual(
    name:add(["/", "file"], Nodes, Names), 
    {ok, #dir{name="/", childs=[#file{name="file", nodes=Nodes}]}}
  ).

add_in_new_path({Names, Nodes}) ->
  ?_assertEqual(
    name:add(["/", "my", "file"], Nodes, Names), 
    {ok, #dir{name="/", childs=[#dir{name="my", childs=[#file{name="file", nodes=Nodes}]}]}}
).

cant_remove_root({Names}) ->
  ?_assertEqual(Names,name:remove(["/"], Names)).

remove_inexistent_file({Names}) ->
  ?_assertEqual(Names, name:remove(["/", "file", "not", "found"], Names)).

remove_single_file({Names, Nodes}) ->
  {ok, NewNames} = name:add(["/", "pepe"], Nodes, Names),
  ?_assertEqual({ok, Nodes, name:init()}, name:remove(["/", "pepe"], NewNames)).

remove_file_and_path({Names, Nodes}) ->
  {ok, NewNames} = name:add(["/", "deep", "pepe"], Nodes, Names),
  ?_assertEqual({ok, Nodes, name:init()}, name:remove(["/", "deep", "pepe"], NewNames)).
  
remove_file_with_siblin({Names, Nodes}) ->
  {ok, NewNames} = name:add(["/", "deep", "pepe1"], Nodes, Names),
  {ok, NewNewNames} = name:add(["/", "deep", "pepe2"], Nodes, NewNames),

  ExpectedNames = #dir{name="/", childs=[#dir{name="deep", childs=[#file{name="pepe2", nodes=Nodes}]}]},
  ?_assertEqual({ok, Nodes, ExpectedNames}, name:remove(["/", "deep", "pepe1"], NewNewNames)).

file_not_found_with_empty_names({Names}) ->
  ?_assertEqual({error, not_found}, name:nodes_by_file(["/", "not", "found"], Names)).

file_not_found_with_some_existent_files({File1, File2, Names}) ->
  ?_assertEqual({error, not_found}, name:nodes_by_file(["/", "not", "found"], Names)).

found_nodes({_, {FilePath, Nodes}, Names}) ->
  ?_assertEqual({ok, Nodes}, name:nodes_by_file(FilePath, Names)).

node_not_found_empty_names({Names}) ->
  ?_assertEqual({error, not_found}, name:files_by_node("not existent node", Names)).

node_not_found_with_some_existent_files({_, _, Names}) ->
  ?_assertEqual({error, not_found}, name:files_by_node("not existent node", Names)).

find_node_in_one_file({{File, [Node | _]}, _, Names}) ->
  ?_assertEqual({ok, [File]}, name:files_by_node(Node, Names)).

find_node_in_two_files({{File, [Node | _]}, _, Names}) ->
  NewFile = ["/", "otro"],
  {ok, NewNames} = name:add(NewFile, [Node], Names),
  ?_assertEqual({ok, [File, NewFile]}, name:files_by_node(Node, NewNames)).

cant_replace_node_of_inexistent_file({{File, _}, _, Names}) ->
  Replaced = name:replace_node(lists:append(lists:droplast(File), ["something"]), "lala", "lele", Names),
  ?_assertEqual({error, not_found, Names}, Replaced).

replacing_not_existent_node({{File, Nodes}, {OtherFile, OtherNodes}, Names}) ->
  NewNode = "a new node",
  Replaced = name:replace_node(File, NewNode, "a inexistent node", Names),
  {ok, NewNames} = name:add(File, [NewNode | Nodes], name:init()),
  {ok, ExpectedNames} = name:add( OtherFile, OtherNodes, NewNames),
  ?_assertEqual({ok, ExpectedNames}, Replaced).

replacing_existent_node({{File, [Node | Nodes]}, {OtherFile, OtherNodes}, Names}) ->
  NewNode = "a new node",
  Replaced = name:replace_node(File, NewNode, Node, Names),
  {ok, NewNames} = name:add(File, [NewNode | Nodes], name:init()),
  {ok, ExpectedNames} = name:add( OtherFile, OtherNodes, NewNames),
  ?_assertEqual({ok, ExpectedNames}, Replaced).
  
%% FIXTURES %%
init_test_() ->
  ?_assertEqual(name:init(), #dir{name="/", childs=[]}).

add_test_() ->
  [
    {setup, fun init_and_nodes/0, fun add_in_new_path/1},
    {setup, fun init_and_nodes/0, fun add_in_existent_path/1},
    {setup, fun init_and_nodes/0, fun add_with_empty_path_fails/1},
    {setup, fun init_and_nodes/0, fun add_with_existent_file_in_path_step_fails/1},
    {setup, fun init_and_nodes/0, fun cant_add_file_in_root/1},
    {setup, fun init_and_nodes/0, fun add_in_existent_dir_fails/1},
    {setup, fun init_and_nodes/0, fun add_siblin/1},
    {setup, fun existent_file_and_nodes/0, fun add_in_existent_file_fails/1}
  ].

remove_test_() ->
  [
    {setup, fun init/0, fun cant_remove_root/1},
    {setup, fun init/0, fun remove_inexistent_file/1},
    {setup, fun init_and_nodes/0, fun remove_single_file/1},
    {setup, fun init_and_nodes/0, fun remove_file_with_siblin/1},
    {setup, fun init_and_nodes/0, fun remove_file_and_path/1}
  ].

nodes_by_file_test_() ->
  [
    {setup, fun init/0, fun file_not_found_with_empty_names/1},
    {setup, fun with_existent_files/0, fun file_not_found_with_some_existent_files/1},
    {setup, fun with_existent_files/0, fun found_nodes/1}
  ].

files_by_node_test_() ->
  [
    {setup, fun init/0, fun node_not_found_empty_names/1},
    {setup, fun with_existent_files/0, fun node_not_found_with_some_existent_files/1},
    {setup, fun with_existent_files/0, fun find_node_in_one_file/1},
    {setup, fun with_existent_files/0, fun find_node_in_two_files/1}
  ].

replace_node_test_() ->
  [
    {setup, fun with_existent_files/0, fun cant_replace_node_of_inexistent_file/1},
    {setup, fun with_existent_files/0, fun replacing_not_existent_node/1},
    {setup, fun with_existent_files/0, fun replacing_existent_node/1}
  ].